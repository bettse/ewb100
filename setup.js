const fetch = require('node-fetch');


const host = process.argv[2];

async function reset(host) {
  const response = await fetch(`http://${host}/reset`)
  const body = await response.text()
  if (response.ok) {
    console.log(body);
  } else {
    throw new Error(body)
  }
}

async function sendCommand(host, command) {
  const cmdString = command.split(' ').join('+');
  const url = `http://${host}/clicmd?cmd=${cmdString}`
  console.log({cmdString, url})
  const response = await fetch(url)
  const body = await response.text()
  if (response.ok) {
    console.log('---- Response ----');
    console.log(body)
    console.log('------------------');
  } else {
    throw new Error(body)
  }
}

async function wifiSetup(host) {
  try {
    await sendCommand(host, 'ver')
    await sendCommand(host, 'set radio ess Oxford')
    await sendCommand(host, 'set radio security aesccmp')
    await sendCommand(host, 'set radio password ericbetts')
    await sendCommand(host, 'set network mode dhcp')
    await reset(host);
  } catch (e) {
    return e.message
  }
  return "success"
}


wifiSetup(host).then(console.log)
