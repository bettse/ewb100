const dgram = require('dgram');

const statusPort = 48888

var known_devices = [];

async function statusWatcher() {
  const socket = dgram.createSocket('udp4');

  try {
    const host = process.argv[2];
  } catch (e) {
    console.log(e.message);
  }

  socket.on('listening', function () {
    const address = socket.address();
    console.log('UDP socket listening on ' + address.address + ":" + address.port);
  });

  socket.on('message', function (message, remote) {
    console.log('Status from', remote.address)
    console.log(message.toString());
    console.log('-----------------------------')
    if (!known_devices.includes(remote.address)) {
      console.log('Adding to known_devices')
      known_devices.push(remote.address)
    }
  });

  socket.bind(statusPort)
  return 'ready'
}

/*
// Add to a udp multicast listener so any new device transmitting is confused to send status
if (!known_devices.includes(remote.address)) {
  // TODO: use setTimeout to defer until audio has been off for a few seconds
  console.log('Configure device status')
  await sendCommand(host, `set airb ip_deployment 10.0.1.159`)
  await sendCommand(host, `set airb port_deployment ${statusPort}`)
  await sendCommand(host, 'set airb deployrecord auto')

  console.log('Adding to known_devices')
  known_devices.push(remote.address)
}
*/


statusWatcher().then(console.log)

